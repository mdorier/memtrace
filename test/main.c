#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void f() {
	void* ptr = malloc(64);
	free(ptr);
}

int main() {
	void* ptr = malloc(32);
	free(ptr);
	f();
	return 0;
}
