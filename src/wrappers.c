#define _GNU_SOURCE
#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>

#ifdef USE_LIBUNWIND
#include "libunwind_backtrace.h"
#endif

#define MAX_BACKTRACE_DEPTH 10

static int bypass_tracing = 0;

static void print_trace(void)
{
	size_t size;
	char **strings;
	size_t i;

#ifndef USE_LIBUNWIND
	void *array[MAX_BACKTRACE_DEPTH];
	size = backtrace(array, MAX_BACKTRACE_DEPTH);
	strings = backtrace_symbols(array, size);
#else
	size = MAX_BACKTRACE_DEPTH;
	strings = libunwind_backtrace(&size);
#endif
	fprintf(stderr," backtrace={");
	for(i = 0; i < size; i++)
		if(i >= 2) fprintf(stderr,"%s;", strings[i]);
	fprintf(stderr,"}");
	free(strings);
}

void* malloc(size_t size)
{
	static void* (*real_malloc)(size_t) = NULL;
	if (!real_malloc)
		real_malloc = dlsym(RTLD_NEXT, "malloc");

	void *p = real_malloc(size);
	if(!bypass_tracing) {
		bypass_tracing = 1;
		fprintf(stderr, "[MEMTRACE:MALLOC] ptr=%p size=%lu", p, (unsigned long)size);
		print_trace();
		fprintf(stderr, "\n");
		bypass_tracing = 0;
	}
	return p;
}

void free(void* ptr)
{
	static void (*real_free)(void*) = NULL;
	if(!real_free)
		real_free =  dlsym(RTLD_NEXT, "free");

	if(!bypass_tracing) {
		bypass_tracing = 1;
		fprintf(stderr, "[MEMTRACE:FREE] ptr=%p ",ptr);
		print_trace();
		fprintf(stderr, "\n");
		bypass_tracing = 0;
	}
	real_free(ptr);
}
