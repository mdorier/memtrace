# Installing #

```
mkdir build
cd build
cmake .. -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=/prefix/to/install
```

# Using #

The **memtrace** program installed should be in your PATH.

```
memtrace ./myprogram args
```